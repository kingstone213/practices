#include <iostream>
#include <typeinfo>

using namespace std;

int main() {
	double result;
	string answer;

	while (1)
	{
		double num1, num2 = 0;

		cout << "Write in two numbers\n";

		cin >> num1;
		cin >> num2;

		if (num1)
		{

			cout << "Would you like to Add, Subtract, Multiply or Divide the numbers?\n";

			cin >> answer;

			if (answer == "Add")
			{
				result = num1 + num2;
				break;
			}
			else if (answer == "Subtract")
			{
				result = num1 - num2;
				break;
			}
			else if (answer == "Multiply")
			{
				result = num1 * num2;
				break;
			}
			else if (answer == "Divide")
			{
				result = num1 / num2;
				break;
			}
			else 
			{
				cout << "Issue\n";
			}
		}
		else
		{
			cout << "Issue\n"; //loops back and forth
		}
	}
	cout << "Your result is " << result << "!\n";
}