#include <iostream>

using namespace std;

int main()
{
	int pass1 = 123, pass2 = 456;
	int pass;

	cout << "Please enter your password!\n";
	cin >> pass;
	
	if (cin)
	{
		if (pass == pass1 || pass == pass2)
		{
			cout << "Access granted!\n";
		}
		else
		{
			cout << "Password denied!\n";
		}
	}
	else
	{
		cout << "Something went wrong!\n";
		return 0;
	}
}