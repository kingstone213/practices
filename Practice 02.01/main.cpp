#include <iostream>
#include <string>

using namespace std;

void main()
{
	string name;

	cout << "Type in your name\n";
	getline(cin, name);
	cout << "Your name is " << name << "!\n";
}