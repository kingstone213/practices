#include <iostream>

using namespace std;

int main()
{
	int num1;
	int num2;

	cout << "type in a number\n";
	cin >> num1;
	cout << "type in a second number\n";
	cin >> num2;

	int sum = num1 + num2;

	cout << "The sum of the numbers is " << sum << "!\n";
}